module.exports = [
    {
        type: 'group',
        metadata: {
            groupName: 'group1'
            lastUpdated: (new Date()-6).valueOf(),
            lastCommenter: 'adam',
            lastMessage: 'message1',
            unreadMessages: 3
        }
    },
    {
        type: 'group',
        metadata: {
            groupName: 'group2'
            lastUpdated: (new Date()-2).valueOf(),
            lastCommenter: 'beatrix',
            lastMessage: 'message2',
            unreadMessages: 0
        }
    },
    {
        type: 'group',
        metadata: {
            groupName: 'group3'
            lastUpdated: (new Date()-20).valueOf(),
            lastCommenter: 'constantine',
            lastMessage: 'message3',
            unreadMessages: 54
        }
    },
    {
        type: 'group',
        metadata: {
            groupName: 'group4'
            lastUpdated: (new Date()-40).valueOf(),
            lastCommenter: 'donal',
            lastMessage: 'message4',
            unreadMessages: 0
        }
    },
    {
        type: 'group',
        metadata: {
            groupName: 'group5'
            lastUpdated: (new Date()-40).valueOf(),
            lastCommenter: 'elizabeth',
            lastMessage: 'message5',
            unreadMessages: 1
        }
    },
    {
        type: 'group',
        metadata: {
            groupName: 'group6'
            lastUpdated: (new Date()-30).valueOf(),
            lastCommenter: 'fred',
            lastMessage: 'message6',
            unreadMessages: 0
        }
    },
    {
        type: 'chat',
        metadata: {
            lastUpdated: (new Date()-10).valueOf(),
            recipient: 'adam',
            lastMessage: 'message1',
            unreadMessages: 0
        }
    },
    {
        type: 'chat',
        metadata: {
            lastUpdated: (new Date()-3).valueOf(),
            recipient: 'constantine',
            lastMessage: 'message2',
            unreadMessages: 2
        }
    },
    {
        type: 'chat',
        metadata: {
            lastUpdated: (new Date()-6).valueOf(),
            recipient: 'donald',
            lastMessage: 'message3',
            unreadMessages: 0
        }
    },
    {
        type: 'chat',
        metadata: {
            lastUpdated: (new Date()-4).valueOf(),
            recipient: 'elizabeth',
            lastMessage: 'message4',
            unreadMessages: 1
        }
    },
    {
        type: 'chat',
        metadata: {
            lastUpdated: (new Date()-13).valueOf(),
            recipient: 'freg',
            lastMessage: 'message5',
            unreadMessages: 0
        }
    },
    {
        type: 'chat',
        metadata: {
            lastUpdated: (new Date()-5).valueOf(),
            recipient: 'greg',
            lastMessage: 'message6',
            unreadMessages: 32
        }
    }
]
