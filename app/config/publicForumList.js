
const hotIcon = require('./../assets/images/icons/share-1.png');
const currentIcon = require('./../assets/images/icons/share-2.png');
const funnyIcon = require('./../assets/images/icons/share.png');
const nsfwIcon = require('./../assets/images/icons/share-1.png');
const gifIcon = require('./../assets/images/icons/share-2.png');
const cuteIcon = require('./../assets/images/icons/share.png');

module.exports = [
    {
        name: 'Hot',
        icon: hotIcon,
        category: 'hot'
    },
    {
        name: 'Current',
        icon: currentIcon,
        category: 'current'
    },
    {
        name: 'Funny',
        icon: funnyIcon,
        category: 'funny'
    },
    {
        name: 'NSFW',
        icon: nsfwIcon,
        category: 'nsfw'
    },
    {
        name: 'Gif',
        icon: gifIcon,
        category: 'gif'
    },
    {
        name: 'Cute',
        icon: cuteIcon,
        category: 'cute'
    }
];
