/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
} from 'react-native';

const TopBarScreen = require('../components/topBarScreen');
const TextField = require('../components/textField');
const Button = require('../components/button');

class Register extends Component {

    register(){

    }

    render() {

        const navigation = {
            title: 'Register',
            rightButtonTitle: 'Main',
            rightButtonAction: ()=>{
                this.props.navigator.push({
                    component: 'Main',
                });
            },
            leftButtonTitle: 'Back',
            leftButtonAction:()=>{
                this.props.navigator.pop();
            }
        }

        const content = (<View style={styles.content}>

            <Image
                style={styles.logo}
                source={require('./../assets/images/logo.png')}
            />

            <TextField
                title={'Username:'}
                update={this.usernameChanged}
                placeholder={'Enter username here'}
                autoFocus={false}
            />

            <TextField
                title={'Email:'}
                update={this.usernameChanged}
                placeholder={'Enter email address here'}
            />

            <TextField
                title={'Password:'}
                update={this.passwordChanged}
                placeholder={'Enter password here'}
                secure={true}
            />

            <TextField
                title={'Confirm Password:'}
                update={this.passwordChanged}
                placeholder={'Enter password here'}
                secure={true}
            />

            <TextField
                title={'Age:'}
                update={this.usernameChanged}
                placeholder={'Enter age here'}
                keyboardType={'numeric'}
            />

            <View style={styles.buttonsContainer}>
                <View style={styles.buttonHolder}>
                    <Button
                        title={'Register'}
                        buttonPressed={this.register.bind(this)}
                        backgroundColor={'#81c04d'}
                        color={'#fff'}
                    />
                </View>
            </View>

        </View>);

        return (<TopBarScreen
            content={content}
            navigation={navigation}
        />);
    }

    //   return (
    //     <View style={styles.container}>
    //       <NavigationBar
    //           navigator={this.props.navigator}
    //           title={'Registration'}
    //           rightButtonTitle={''}
    //           rightButtonAction={()=>{}}
    //       />
    //       <ScrollView style={styles.contentContainer}>
    //           <View style={styles.content}>
      //
    //               <Image
    //                   style={styles.logo}
    //                   source={require('./../assets/images/logo.png')}
    //               />
      //
    //               <TextField
    //                   title={'Username:'}
    //                   update={this.usernameChanged}
    //                   placeholder={'Enter username here'}
    //                   autoFocus={true}
    //               />
      //
    //               <TextField
    //                   title={'Email:'}
    //                   update={this.usernameChanged}
    //                   placeholder={'Enter email address here'}
    //               />
      //
    //               <TextField
    //                   title={'Password:'}
    //                   update={this.passwordChanged}
    //                   placeholder={'Enter password here'}
    //                   secure={true}
    //               />
      //
    //               <TextField
    //                   title={'Confirm Password:'}
    //                   update={this.passwordChanged}
    //                   placeholder={'Enter password here'}
    //                   secure={true}
    //               />
      //
    //               <TextField
    //                   title={'Age:'}
    //                   update={this.usernameChanged}
    //                   placeholder={'Enter age here'}
    //                   keyboardType={'numeric'}
    //               />
      //
    //               <View style={styles.buttonsContainer}>
    //                   <View style={styles.buttonHolder}>
    //                       <Button
    //                           title={'Register'}
    //                           buttonPressed={this.register.bind(this)}
    //                           backgroundColor={'#81c04d'}
    //                           color={'#fff'}
    //                       />
    //                   </View>
    //               </View>
      //
    //           </View>
    //       </ScrollView>
    //     </View>
    //   );
  // }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    contentContainer: {
        flex: 1,
        backgroundColor: '#e4f2d9',
    },
    content: {
        flex: 1,
        padding: 10,
        justifyContent: 'center',
        alignItems: 'center',
    },
    buttonsContainer:{
        flex: 1,
        flexDirection: 'row',
        marginVertical: 20,
        marginHorizontal: -10,
        alignItems: 'center',
    },
    buttonHolder:{
        flex: 0.5,
        paddingHorizontal: 10,
    },
    logo:{
        // marginVertical: 30,
    }
});

module.exports = Register;
