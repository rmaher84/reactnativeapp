/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
} from 'react-native';

const TopBarScreen = require('../components/topBarScreen');
const TextField = require('../components/textField');
const Button = require('../components/button');

class ForumPage extends Component {

    render(){

        const navigation = {
            title: this.props.passedProps,
            leftButtonTitle: 'Back',
            leftButtonAction: ()=>{
                this.props.navigator.pop();
            }
        }

        const content = (<View style={styles.content}>
            <Image
                style={styles.logo}
                source={require('./../assets/images/logo.png')}
            />

            <TextField
                title={'Username:'}
                update={this.usernameChanged}
                placeholder={'Enter username here'}
                autoFocus={false}
            />

            <TextField
                title={'Password:'}
                update={this.passwordChanged}
                placeholder={'Enter password here'}
                secure={true}
            />
        </View>);

        return (<TopBarScreen
            content={content}
            navigation={navigation}
        />);
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    contentContainer: {
        flex: 1,
        backgroundColor: '#e4f2d9',
    },
    content: {
        flex: 1,
        padding: 10,
        justifyContent: 'center',
        alignItems: 'center',
    },
    buttonsContainer:{
        flex: 1,
        flexDirection: 'row',
        marginVertical: 20,
        marginHorizontal: -10
    },
    buttonHolder:{
        flex: 0.5,
        paddingHorizontal: 10,
    },
    logo:{
        marginVertical: 30,
    }
});

module.exports = ForumPage;
