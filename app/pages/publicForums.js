/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  ListView,
  Image,
  TouchableOpacity,
  Dimensions,
} from 'react-native';

const TopBarScreen = require('../components/topBarScreen');

const publicForumList = require('../config/publicForumList');

class Public extends Component {

    constructor(props) {
        super(props);
        var ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});

        this.state = { dataSource: ds.cloneWithRows( publicForumList ) };
    }

    goToCategory(category){
        this.props.navigator.push({
            component: 'ForumPage',
            passedProps: category
        });
    }

    listItem(rowData){
        // var icon = require(rowData.icon);
        // debugger;
        return (<View style={styles.itemRender}>
            <TouchableOpacity onPress={this.goToCategory.bind(this, rowData.category)}>
                <View style={styles.itemRenderContent}>
                    <View style={{height: 60, width: 60, marginRight: 10}}>
                        <Image
                            style={{height: 60, width: 60}}
                            source={rowData.icon}
                        />
                    </View>
                    <View>
                        <Text>{rowData.name}</Text>
                        <Text>{rowData.icon}</Text>
                        <Text>{rowData.category}</Text>
                    </View>
                </View>
            </TouchableOpacity>
        </View>);
    }

    render(){

        const navigation = {
            title: 'Public'
        }

        const content = (<View style={styles.container}>
            <ListView
                style={styles.listViewContainer}
                dataSource={this.state.dataSource}
                renderRow={(rowData) => this.listItem(rowData)}
            />
        </View>);

        return (<TopBarScreen
            content={content}
            navigation={navigation}
        />);
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    listViewContainer:{
        borderTopWidth: 1,
        margin: -10,
    },
    itemRender: {
        width: Dimensions.get('window').width,
        height: 80,
        backgroundColor: '#fff',
        borderBottomWidth: 1,
        padding: 10,
    },
    itemRenderContent:{
        flexDirection: 'row',
    }
});

module.exports = Public;
