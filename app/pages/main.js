/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
} from 'react-native';

const NavigationTabScreen = require('../components/navigationTabScreen');

class Main extends Component {

    render(){

        const tabs = [
            {
                tabName: 'Public',
                tabIcon: '',
                tabPage: require('../pages/publicForums')
            },
            {
                tabName: 'Chat',
                tabIcon: '',
                tabPage: require('../pages/register')
            },
            {
                tabName: 'Memes',
                tabIcon: '',
                tabPage: require('../pages/login')
            },
            {
                tabName: 'Settings',
                tabIcon: '',
                tabPage: require('../pages/register')
            }
        ];

        return (<NavigationTabScreen
            tabs={tabs}
            navigator={this.props.navigator}
        />);
    }
}

const styles = StyleSheet.create({
});

module.exports = Main;
