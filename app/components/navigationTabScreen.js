/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
} from 'react-native';

const NavigationTabs = require('../components/navigationTabs');

class NavigationTabScreen extends Component {

    constructor(props) {
        super(props);

        this.state = {
            content: this.props.tabs[(this.props.selectedTab || 0)].tabPage,
            selectedTab: (this.props.selectedTab || 0)
        };
    }

    selectTab(tabIndex){
        this.setState({
            content: this.props.tabs[tabIndex].tabPage,
            selectedTab: tabIndex
        });
    }

    render() {
        return (
            <View style={styles.container}>
                <this.state.content navigator={this.props.navigator}/>
                <NavigationTabs
                    tabItems={this.props.tabs}
                    selectTab={this.selectTab.bind(this)}
                    selectedTab={this.state.selectedTab}
                />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    }
});

module.exports = NavigationTabScreen;
