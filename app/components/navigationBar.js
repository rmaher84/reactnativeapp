/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity
} from 'react-native';

class NavigationBar extends Component {

    render() {
        return (
            <View style={styles.toolbar}>
                <TouchableOpacity onPress={this.props.leftButtonAction || (()=>{})}>
                    <Text style={styles.toolbarButton}>{this.props.leftButtonTitle || ''}</Text>
                </TouchableOpacity>

                <Text style={styles.toolbarTitle}>{this.props.title || ''}</Text>

                <TouchableOpacity onPress={this.props.rightButtonAction || (()=>{})}>
                    <Text style={styles.toolbarButton}>{this.props.rightButtonTitle || ''}</Text>
                </TouchableOpacity>
            </View>
        );
    }
}

var styles = StyleSheet.create({
    toolbar:{
        backgroundColor:'#81c04d',
        paddingTop:30,
        paddingBottom:10,
        flexDirection:'row'
    },
    toolbarButton:{
        width: 70,
        color:'#fff',
        textAlign:'center'
    },
    toolbarTitle:{
        color:'#fff',
        textAlign:'center',
        fontWeight:'bold',
        flex:1
    }
});

module.exports = NavigationBar;
