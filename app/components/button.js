/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
} from 'react-native';

class Button extends Component {

  render() {

    const backgroundColor = this.props.backgroundColor || '#fff';
    const color = this.props.color || '#000';

    return (
        <View style={[styles.buttonContainer, {backgroundColor}]}>
            <TouchableOpacity style={styles.button} onPress={this.props.buttonPressed}>
                <Text style={[styles.buttonText, {color}]}>
                    {this.props.title}
                </Text>
            </TouchableOpacity>
        </View>
    );
  }
}

var styles = StyleSheet.create({
    buttonContainer:{
        height: 50,
        flex: 1,
        borderRadius: 25,
        borderWidth: 1,
        paddingHorizontal: 20
    },
    button:{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    buttonText:{
    }
});

module.exports = Button;
