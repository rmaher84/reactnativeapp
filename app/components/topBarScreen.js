/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  ScrollView,
} from 'react-native';

const NavigationBar = require('../components/navigationBar');
const TextField = require('../components/textField');
const Button = require('../components/button');

class TopBarScreen extends Component {

    navigateToRegistrationPage(){
        this.props.navigator.push({
            name: 'page two',
            component: 'Register',
            passedProps: 'this is the login page!'
        })
    }

  render() {
    return (
      <View style={styles.container}>
        <NavigationBar
            navigator={this.props.navigator}
            title={this.props.navigation.title}
            rightButtonTitle={this.props.navigation.rightButtonTitle}
            rightButtonAction={this.props.navigation.rightButtonAction}
            leftButtonTitle={this.props.navigation.leftButtonTitle}
            leftButtonAction={this.props.navigation.leftButtonAction}
        />
        <ScrollView style={styles.contentContainer}>
            <View style={styles.content}>
                {this.props.content}
            </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    contentContainer: {
        flex: 1,
        backgroundColor: '#e4f2d9',
    },
    content: {
        flex: 1,
        padding: 10,
        justifyContent: 'center',
        alignItems: 'center',
    }
});

module.exports = TopBarScreen;
