/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
} from 'react-native';

class NavigationTabs extends Component {

    createTab(key, tabName, tabIcon, tabWidth, tabPage){
        return (<View key={key} style={[styles.tabItem, {flex: tabWidth}, (key===this.props.selectedTab?{backgroundColor: '#51901d'}:{})]}>
            <TouchableOpacity style={styles.touchable} onPress={()=>{this.props.selectTab(key)}}>
                <Text style={styles.text}>{tabName}</Text>
            </TouchableOpacity>
        </View>);
    }

    render() {

        const tabItems = this.props.tabItems;
        const flexWidth = 1/tabItems.length;
        let key = 0;
        let tabs = [];

        for (var x = 0; x < tabItems.length; x++){
            tabs.push(this.createTab(x,tabItems[x].tabName, tabItems[x].tabIcon, flexWidth));
        }

        return (<View style={styles.tabsContainer}>{tabs}</View>);
    }
}

var styles = StyleSheet.create({
    tabsContainer:{
        height: 60,
        flexDirection: 'row',
        backgroundColor:'#81c04d',
    },
    tabItem:{
        height: 60,
    },
    touchable:{
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    text:{
        color: '#fff',
    }
});

module.exports = NavigationTabs;
