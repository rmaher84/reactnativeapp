/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput
} from 'react-native';

class TextField extends Component {

  render() {
    return (
        <View style={styles.textFieldContainer}>
            <View style={styles.textFieldHeadingContainer}>
                <Text style={styles.textFieldHeading}>
                    {this.props.title}
                </Text>
            </View>
            <TextInput
                style={styles.textFieldInput}
                defaultValue={this.props.default || ''}
                keyboardType={this.props.keyboardType || 'default'}
                onChangeText={this.props.update}
                placeholder={this.props.placeholder || ''}
                secureTextEntry={!!this.props.secure}
                autoFocus={!!this.props.autoFocus}
            />
        </View>
    );
  }
}

var styles = StyleSheet.create({
    textFieldContainer:{
        flexDirection:'row',
        borderWidth: 1,
        height: 50,
        borderRadius: 25,
        justifyContent: 'center',
        paddingHorizontal: 20,
        marginVertical: 10,
        backgroundColor: '#fff',
    },
    textFieldHeadingContainer:{
        width: 100,
        height: 40,
        justifyContent: 'center',
        alignItems: 'center'
    },
    textFieldHeading:{
        textAlign:'left',
        fontWeight:'bold',
        color:'#000',
        marginTop: 7
    },
    textFieldInput:{
        color:'#000',
        textAlign:'left',
        flex:1
    }
});

module.exports = TextField;
