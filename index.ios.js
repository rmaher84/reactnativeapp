/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import KeyboardSpacer from 'react-native-keyboard-spacer';
import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View,
  Navigator
} from 'react-native';

if (!window.location) {
    window.navigator.userAgent = 'ReactNative';
}

const Login = require('./app/pages/login');
const Register = require('./app/pages/register');
const Main = require('./app/pages/main');
const ForumPage = require('./app/pages/forumPage');

const pages = { Login, Register, Main, ForumPage }

class navigationTest2 extends Component {

    constructor(props) {
        super(props);

        const io = require('socket.io-client/socket.io');
        const socketIoClient = new (require('./app/helpers/socketIoClient'))(io);

        this.state = { socketIoClient };
    }

    componentDidMount(){

        console.log('connecting to server');
        let userId = 0;
        const socketIoClient = this.state.socketIoClient;

        socketIoClient.connectToServer('user'+userId, 'user'+userId, null, function(error, response){

            if (!error){
                console.log('logged in single users.');
            } else {
                console.log('failed to log in single user. trying again.');
            }

        });
    }

    render() {
          return (
              <Navigator
                  initialRoute={{component: 'Login'}}
                  configureScene={() => {
                      return Navigator.SceneConfigs.FloatFromRight;
                  }}
                  renderScene={(route, navigator) => {

                      if (!!pages[route.component]) {
                          return React.createElement(View, {style:styles.container},
                              React.createElement(pages[route.component], {
                                  navigator: navigator,
                                  passedProps: route.passedProps
                              }),
                              React.createElement(KeyboardSpacer,{})
                          );
                      }
                  }}
               />
          );
      }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

AppRegistry.registerComponent('navigationTest2', () => navigationTest2);
